package com.solomia;

import com.solomia.view.MyView;

public class Application {
    public static void main(String[] args) {
        new MyView().show();
    }
}
