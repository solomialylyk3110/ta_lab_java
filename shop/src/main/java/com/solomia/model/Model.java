package com.solomia.model;

import com.solomia.model.Goods;

import java.util.List;

public interface Model {
    List<Goods> findByGroup(String name, List<Goods> tov);


    List<Goods> SortByPrize(int amount, List<Goods> tov);
}
