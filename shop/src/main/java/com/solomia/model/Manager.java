package com.solomia.model;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Manager implements Model {
    public List<Goods> findByGroup(String name, List<Goods> tov) {
        List<Goods> result = new LinkedList<>();
        Iterator<Goods> it = tov.iterator();
        while (it.hasNext()) {
            Goods current = it.next();
            if(current.getName().equals(name)) {
                result.add(current);
            }
        }
        return result;
    }


    public List<Goods> SortByPrize(int amount, List<Goods> tov) {
        ArrayList<Goods> smallPrices = new ArrayList();
        Iterator<Goods> it = tov.iterator();
        while (it.hasNext()) {
            Goods current = it.next();
            if(current.getPrize() <= amount) {
                smallPrices.add(current);
            }
        }
        return smallPrices;

    }
}
