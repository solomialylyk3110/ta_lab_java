package com.solomia.model;

public abstract class Goods {
    String name;
    String color;
    int prize;

    public Goods(String name, String color, int prize) {
        this.name = name;
        this.color = color;
        this.prize = prize;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getPrize() {
        return prize;
    }

    public void setPrize(int prize) {
        this.prize = prize;
    }


    @Override
    public String toString() {
        return "Goods{" +
                "name='" + name + '\'' +
                ", color='" + color + '\'' +
                ", prize=" + prize +
                '}'+ '\n';
    }

}
