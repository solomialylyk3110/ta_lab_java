package com.solomia.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LogicAndFillings{
    public List<Goods> tov;

    public LogicAndFillings() {
        tov = new ArrayList<>();
        addGoods();

    }

    private void addGoods() {
        tov= Arrays.asList(
                new Plumbing("washbasin", "white", 3400, " ceramic"),
                new Plumbing("washbasin", "grey", 2900, " ceramic"),
                new Plumbing("toilet", "white", 5400, " ceramic"),
                new WoodenProduct("door", "brown", 5400, " oak"),
                new WoodenProduct("door", "black", 3000, " linden"));
    }

}
