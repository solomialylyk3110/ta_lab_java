package com.solomia;

import com.solomia.view.JavaSourceCodeReader;
import com.solomia.view.MyView;

import java.io.*;

public class App {
    public static void main(String[] args) throws IOException {
        //new MyView().usualReader();
        new JavaSourceCodeReader().findComments();
    }
}
