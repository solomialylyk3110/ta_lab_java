package com.solomia.path;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PathToFile {
    public String getPath(String namePath) {
        Properties prop = new Properties();
        try (InputStream input = new FileInputStream("IO-NIO\\src\\main\\resources\\path.properties")) {
            prop.load(input);
        } catch (IOException io) {
            io.printStackTrace();
        }
        return prop.getProperty(namePath);
    }
}
