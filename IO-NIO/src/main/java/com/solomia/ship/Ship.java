package com.solomia.ship;

import java.io.Serializable;

public class Ship implements Serializable {
    String nameShip;
    private Droid droid;
    public static int  value;
    private transient int transientValue;

    public Ship(String nameShip, Droid droid, int transientValue) {
        this.nameShip = nameShip;
        this.droid = droid;
        this.transientValue = transientValue;

    }


    public String getNameShip() {
        return nameShip;
    }

    public void setNameShip(String nameShip) {
        this.nameShip = nameShip;
    }

    public Droid getDroid() {
        return droid;
    }

    public void setDroid(Droid droid) {
        this.droid = droid;
    }

    public static int getValue() {
        return value;
    }

    public static void setValue(int value) {
        Ship.value = value;
    }

    public int getTransientValue() {
        return transientValue;
    }

    public void setTransientValue(int transientValue) {
        this.transientValue = transientValue;
    }

    @Override
    public String toString() {
        return "Ship{" +
                "nameShip='" + nameShip + '\'' +
                ", droid=" + droid +
                ", transientValue=" + transientValue +
                ", StaticValue=" + value +
                '}';
    }
}
