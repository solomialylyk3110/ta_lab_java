package com.solomia.view;

import com.solomia.App;
import com.solomia.path.PathToFile;
import com.solomia.ship.Droid;
import com.solomia.ship.Ship;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Paths;

public class MyView {
    private static final Logger logger = LogManager.getLogger(App.class);
    private static final String PATH = new PathToFile().getPath("javaSourceCodeReading");

    public void usualReader() throws IOException {
        int count = 0;
        logger.info("Test usual reader");
        InputStream inputStream = new FileInputStream(String.valueOf(Paths.get(PATH)));
        int data = inputStream.read();
        while (data != -1) {
            data = inputStream.read();
            count++;
        }
        inputStream.close();
        logger.info("count = " + count);
    }

    public void bufferedReader() throws IOException {
        int buf = 1 * 1024 * 1024; // can without it (1 MB buffer)
        int count = 0;
        logger.info("Test buffered reader");
        DataInputStream in = new DataInputStream(
                new BufferedInputStream(new FileInputStream(new PathToFile().getPath("fileForReading")), buf));
        while (in.available() > 0) {
            byte b = in.readByte();
            count++;
        }
        in.close();
        logger.info("count = " + count);
    }

    public void showContentsOfSpecificDirectory() throws IOException {
        logger.info("contents of a specific directory");
        File file = new File("D:\\Other");
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            logger.error("Directory does not exist");
        }
    }

    private void printDirectory(File file, String s) {
        logger.info(s + "Directory ");
        s = s + " ";
        File[] fileName = file.listFiles();
        for (File f : fileName) {
            if (f.isDirectory()) {
                printDirectory(f, s);
            } else {
                logger.info(s + " File " + f.getName());
            }
        }
    }

    public void NIOExample() throws IOException {
        RandomAccessFile aFile =
                new RandomAccessFile(new PathToFile().getPath("fileForReading"), "rw");
        FileChannel inChannel = aFile.getChannel();
        //create buffer with capacity of 48 bytes
        ByteBuffer buf = ByteBuffer.allocate(48);
        int bytesRead = inChannel.read(buf); //write into buffer.
        while (bytesRead != -1) {
            buf.flip();  //make buffer ready for read from
            while (buf.hasRemaining()) {
                logger.info((char) buf.get()); // read 1 byte at a time
            }
            buf.clear(); //make buffer ready for writing into
            bytesRead = inChannel.read(buf);
        }
        aFile.close();
    }

    public void serialization() {
        Ship.value = 90;
        Ship ship = new Ship("Cosmo-32", new Droid("Droid1", 123), 12);
        logger.info(ship);
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream("Ship.dat"));
            out.writeObject(ship);
            out.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //Ship.value=100;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream("Ship.dat"));
            Ship ship1 = (Ship) in.readObject();
            in.close();
            logger.info(ship1);
        } catch (FileNotFoundException e) {
            logger.error("File not found", e);
        } catch (IOException e) {
            logger.error(e);
        } catch (ClassNotFoundException e) {
            logger.error(e);
        }
    }
}
