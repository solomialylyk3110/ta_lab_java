package com.solomia.view;

import com.solomia.App;
import com.solomia.path.PathToFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class JavaSourceCodeReader {
    private static Logger logger = LogManager.getLogger(App.class);
    private static final String PATH = new PathToFile().getPath("javaSourceCodeReading");

    public String read() throws IOException {
        String text = "";
        try {
            text = new String(Files.readAllBytes(Paths.get(PATH)));
        } catch (IOException e) {
            logger.error("File not found!");
        }
        return text;
    }

    public void findComments() throws IOException {
        String text = read();
        String comment="";
        for (int i = 0; i<text.length(); i++) {
            if (text.charAt(i)=='/') {
                do {
                    comment +=text.charAt(i);
                    System.out.print(text.charAt(i));
                    i++;
                } while (!(text.charAt(i) =='\n'));
            }
        }
    }
}
