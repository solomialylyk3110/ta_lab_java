package com.solomia.container;

public class Container<T extends Product>{

    T item;

    public T getItem() {
        return item;
    }

    public void setItem(T item) {
        this.item = item;
    }

    @Override
    public String toString() {
        return "Container{" +
                "item=" + item +
                '}';
    }
}
