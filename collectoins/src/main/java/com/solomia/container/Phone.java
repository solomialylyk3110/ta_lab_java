package com.solomia.container;

public class Phone extends Product {
    String model;

    public Phone(String name, int price, String model) {
        super(name, price);
        this.model = model;
    }

    public Phone() {
    }

    @Override
    public String toString() {
        return "Phone{" +
                "model='" + model + '\'' +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
