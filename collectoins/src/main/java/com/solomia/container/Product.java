package com.solomia.container;

public abstract class Product<T> {
    String name;
    int price;

    public Product(String name, int price) {
        this.name = name;
        this.price = price;
    }

    public Product() {
    }
}
