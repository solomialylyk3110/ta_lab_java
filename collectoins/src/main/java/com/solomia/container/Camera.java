package com.solomia.container;

public class Camera extends Product {
    int pixels;

    public Camera(String name, int price, int pixels) {
        super(name, price);
        this.pixels = pixels;
    }

    public Camera() {
        super();
    }

    @Override
    public String toString() {
        return "Camera{" +
                "pixels=" + pixels +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}

