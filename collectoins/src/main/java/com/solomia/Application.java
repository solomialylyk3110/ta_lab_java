package com.solomia;

import com.solomia.container.Camera;
import com.solomia.container.Container;
import com.solomia.container.Phone;
import com.solomia.container.Product;

import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        Product ph = new Phone("samsung", 8000, "3425");
        print(ph);
        Product cam = new Camera("ni", 36000, 1000);
        print(cam);

        Container<Product> c = new Container<>();
        c.setItem(new Camera("niiiii", 36000, 1000));
        c.getItem();
        print(c);

        List<Camera> cameras = new ArrayList<Camera>();
        find(cameras, new Camera());
        find(cameras, new Phone());

        find2(cameras, new Camera());
        // find2(cameras, new Phone());
        List<Phone> phones = new ArrayList<Phone>();
        find2(phones, new Phone());
    }

    static boolean find(List<? extends Product> all, Product p) {
        return true;
    }

    static <T extends Product> boolean find2(List<T> all, T p) {
        return true;
    }

    static <T> void print(T product) {
        System.out.println(product);
    }
}
