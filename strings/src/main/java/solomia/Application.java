package solomia;

import solomia.logic.MainLogic;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        MainLogic logic= new MainLogic();
        logic.replaceWordByLength(0,4, "solomia");
        logic.countSentenceRepeatWord();
        logic.showSentenceByWordNumber();
        //logic.interrogativeSentences(2);
        //System.out.println("Alpha:");
        //logic.alphabetSort();
    }
}
