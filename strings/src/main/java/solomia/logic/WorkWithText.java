package solomia.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.Application;
import solomia.model.Sentence;
import solomia.model.Text;
import solomia.model.Word;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WorkWithText {
    private static Logger logger = LogManager.getLogger(Application.class);

    public Text createText() {
        String mytext = "";
        try {
            mytext = new FileReading().readingText();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Text text = new Text(mytext);
        text.setSentencesList(generateSentenceList(text.getText()));
        text.setWordsList(geterateWordList(text.getText()));
        text.getSentencesList().forEach(s -> s.setWordList(geterateWordList(s.getSentence())));
        return text;
    }

    private List<Word> geterateWordList(String text) {
        List<Word> wordList = new ArrayList<>();
        Pattern pattern = Pattern.compile("[A-Za-zА-Яа-я]+");//("([^\\\\s,.!?]+)");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String str = matcher.group();
            wordList.add(new Word(str));

        }
        System.out.println(wordList);
        return wordList;
    }

    private List<Sentence> generateSentenceList(String text) {
        List<Sentence> sentenceList = new ArrayList<>();
        Pattern pattern = Pattern.compile("[^.?!]+[.?!]");
        Matcher matcher = pattern.matcher(text);
        while (matcher.find()) {
            String str = matcher.group();
            sentenceList.add(new Sentence(str));
        }
        return sentenceList;
    }
}
