package solomia.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.Application;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileReading {
    private static final String PATH = "E:\\IntelijIdea\\ta_lab_java\\strings\\src\\main\\resources\\1.txt";
    private static Logger logger = LogManager.getLogger(Application.class);

    public String readingText() throws IOException {
        String text = "";
        try {
            text = new String(Files.readAllBytes(Paths.get(PATH)));
        } catch (IOException e) {
            logger.error("File not found!");
        }
        System.out.println(text);
        return text;
    }
}
