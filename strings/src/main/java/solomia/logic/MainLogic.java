package solomia.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import solomia.Application;
import solomia.model.Sentence;
import solomia.model.Text;
import solomia.model.Word;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class MainLogic {
    private Text text;
    private List<Sentence> sentences;
    private List<Word> words;
    private static Logger log = LogManager.getLogger(Application.class);

    public MainLogic() {
        WorkWithText workWithText = new WorkWithText();
        this.text = workWithText.createText();
        this.sentences = text.getSentencesList();
        this.words = text.getWordsList();
    }

    public void alphabetSort() {
        List<Word> wordList = words.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println(wordList);
    }

    public void interrogativeSentences(int size) {
        List<Sentence> sentenceList = sentences.stream()
                .filter(s -> s.getSentence().matches("[^.?!]+[?]"))
                .collect(Collectors.toList());
        sentenceList.stream()
                .forEach(s1 -> s1.getWordList()
                        .stream()
                        .distinct()
                        .filter(word -> word.getWord().length() == size)
                        .forEach(word -> log.info(word.getWord())));
    }

    public void sortedWordsInSentences() {
        System.out.println(sentences);
        sentences.stream()
                .forEach(s1 -> s1.getWordList()
                        .stream()
                        .distinct()
                        .sorted()
                        .forEach(word -> log.info(word.getWord())));
    }

    public void deleteWordsByLength(int length) {
        List<Word> wordVowelList = words.stream()
                .filter(word -> word.getWord().matches("[aeiouAEIOU\\s]+[a-zA-Z]*"))
                .collect(Collectors.toList());
        List<Word> wordList = words.stream()
                .filter(word -> word.getWord().matches("[^aeiouAEIOU\\s]+[a-zA-Z]*")
                        && word.getWord().length() != length)
                .collect(Collectors.toList());
        wordList.addAll(wordVowelList);
        wordList.stream()
                .forEach(word -> log.info(word));
    }

    public void replaceWordByLength(int numberSentence, int lengthWord, String str) {
        if (str.length() == lengthWord) {
            log.warn("Equals length of words");
        } else {
            String firstSentence = sentences.get(numberSentence).getSentence().replaceAll("\\b[\\w]{" + lengthWord + "}\\b", str);
            log.info(firstSentence);
        }
    }

    public void countSentenceRepeatWord() {
        int count;
        count = (int) sentences.stream()
                .filter(s -> s.getWordList().size() != s.getWordList()
                        .stream()
                        .distinct()
                        .count())
                .count();
        log.info(count);
    }

    public void showSentenceByWordNumber() {
        Comparator<Sentence> comparator = Comparator.comparing(s1 -> s1.getWordList().size());
        sentences.stream()
                .sorted(comparator)
                .forEach(sentence -> log.info(sentence.getSentence()));
    }
}
