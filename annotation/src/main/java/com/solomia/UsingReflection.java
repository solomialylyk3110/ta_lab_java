package com.solomia;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class UsingReflection {
    public void printFields() {
        Class clazz = MyClass.class;
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (field.isAnnotationPresent(MyField.class)) {
                MyField myField = field.getAnnotation(MyField.class);
                System.out.println(" Field " + field.getName());
                System.out.println(" name in @MyField " + myField.name());
                System.out.println(" age in @MyField " + myField.age());
            }
        }
    }

    public void printMethods() {
        Class clazz = MyClass.class;
        Method[] methods = clazz.getDeclaredMethods();
        for(Method method: methods) {
            System.out.println(method.getName() + " - " + method.getReturnType());
        }
        try {
            Method methodCall = clazz.getDeclaredMethod("getInfo");
            methodCall.setAccessible(true);
            methodCall.invoke(clazz);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void printWithDifferentParameters() {
        MyClass myClass = new MyClass();
        Class clazz = myClass.getClass();
        String[] strings = {"one", "two", "three"};
        try {
            Method method = clazz.getDeclaredMethod("myMethodstring", new Class[]{String.class, String[].class});
            method.setAccessible(true);
            String[] strings1 = (String[]) method.invoke(myClass, "hi", strings);
            for (String str : strings1) {
                System.out.println(str);
            }
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    public void printWithUnknownObject(){
        MyClass myClass = new MyClass();
        reflUnknownObject(myClass);
    }

    private void reflUnknownObject(Object object) {
        try {
            Class clazz = object.getClass();
            System.out.println("The name of class is: " + clazz.getSimpleName());
            Constructor constructor = clazz.getConstructor();
            System.out.println("The name of constructor: " + constructor.getName());
            System.out.println("The methods of class are: ");
            Method[] methods = clazz.getMethods();
            for (Method method : methods) {
                System.out.println(method.getName());
            }
            System.out.println();
            Field[] fields = clazz.getDeclaredFields();
            for (Field field : fields) {
                System.out.println(field.getName());
            }
            fields[0].setAccessible(true);
            if (fields[0].getType() == int.class) {
                fields[0].setInt(object, 47);
            }
            System.out.println(object);
            Method methodCall = clazz.getDeclaredMethod("getInfo");
            methodCall.setAccessible(true);
            methodCall.invoke(object);

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }



}
