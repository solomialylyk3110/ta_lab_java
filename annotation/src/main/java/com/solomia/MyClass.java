package com.solomia;

public class MyClass {
    @MyField(age = 20)
    private String name;
    private int integerValue;
    private double doubleValue;

    private void getInfo() {
        System.out.println("Hello from Solomia");
    }

    public String[] myMethodstring(String str, String... args) {
        String[] strings = new String[args.length];
        for (int i = 0; i < args.length; i++) {
            strings[i] = str + " -> " + args[i] + " -> " + i;
        }
        return strings;
    }

    public int getIntegerValue() {
        return integerValue;
    }

    public void setIntegerValue(int integerValue) {
        this.integerValue = integerValue;
    }

    public double getDoubleValue() {
        return doubleValue;
    }

    public void setDoubleValue(double doubleValue) {
        this.doubleValue = doubleValue;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "name='" + name + '\'' +
                ", integerValue=" + integerValue +
                ", doubleValue=" + doubleValue +
                '}';
    }
}
