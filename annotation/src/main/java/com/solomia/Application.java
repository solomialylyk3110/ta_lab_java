package com.solomia;

public class Application {
    public static void main(String[] args) {
        UsingReflection usingReflection = new UsingReflection();
        usingReflection.printFields();
        //usingReflection.printMethods();
        //usingReflection.printWithUnknownObject();
        usingReflection.printWithDifferentParameters();
    }
}
