package task1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Input length : ");
            a = scanner.nextInt();
            System.out.println("Input width : ");
            b = scanner.nextInt();
            CalculationImpl calculation = new CalculationImpl();
            int result = calculation.calculate(new Restangle(a, b));
            System.out.println("Result: " + result);
        } catch (InputMismatchException e) {
            System.out.println("Incorrect input!");
        }

    }
}
