package task1;

public class CalculationImpl{

    public int calculate(Restangle restangle) {
        return restangle.getLength()+restangle.getWidth();
    }

    @Deprecated
    public boolean isNumeric(String s) {
        return java.util.regex.Pattern.matches("\\d+", s);
    }
}
