package task2.enums;

public enum Type {
    DAISY, TULIP, ROSE, ORCHID, CHRYSANTHEMUM
}
