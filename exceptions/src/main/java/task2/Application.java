package task2;

import task2.enums.Color;
import task2.enums.Type;
import task2.myExceptions.ColorException;
import task2.myExceptions.TypeException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        List<Plant> plantList = new ArrayList<>();
        plantList= Arrays.asList(new Plant(Type.CHRYSANTHEMUM, Color.BLUE, 12),
                new Plant(Type.CHRYSANTHEMUM, Color.RED, 18),
                new Plant(Type.DAISY, Color.PURPLE, 10),
                new Plant(Type.ORCHID, Color.RED, 28),
                new Plant(Type.ROSE, Color.RED, 25)
                );
        System.out.println("List of plants:");
        for (Plant plant: plantList) {
            System.out.println(plant);
        }
        try {
            for (Plant plant: plantList) {
                if(plant.getColor().equals(Color.GREEN)){
                    throw new ColorException("Color can't be green");
                }
                if(plant.getType().equals(Type.CHRYSANTHEMUM)){
                    throw new TypeException("We haven't Chrysanthemum");
                }
            }
        } catch (ColorException | TypeException e) {
            System.err.println(e);
        }
    }
}
