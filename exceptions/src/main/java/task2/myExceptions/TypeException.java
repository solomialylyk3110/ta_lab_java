package task2.myExceptions;

public class TypeException extends Exception {
    public TypeException(String message) {
        super(message);
    }

    public TypeException() { }
}
