package task2.myExceptions;

import com.sun.xml.internal.ws.api.message.Message;

public class ColorException extends Exception {
    public ColorException(String message) {
        super(message);
    }

    public ColorException() {}
}
