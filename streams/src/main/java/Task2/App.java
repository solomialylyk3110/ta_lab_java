package Task2;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Map<String, Command> avalableCom = new HashMap<>();
        avalableCom.put("1", new Command() {
            @Override
            public void execute(String value) {
                System.out.println(value);
            }
        });

        avalableCom.put("2", value -> System.out.println(value));
        avalableCom.put("3", new MyClass()::print);
        avalableCom.put("4", MyClass::printStatic);
        Scanner scan = new Scanner(System.in);
        System.out.println("Input number from 1 to 4");
        String nameKey=scan.next();
        System.out.println("Input your string");
        String nameString=scan.next();
        avalableCom.get(nameKey).execute(nameString);
    }
}
//package Task2;
//interface Command {
//    static void printWithLambda(String name);
//
//    void printWithReferences(String name);
//
//    void printWithAnon(String name);
//
//    void printWithObject(String name);
//
//}
//public static class CommandImpl implements Command {
//
//    public static void printWithLambda(String name) {
//        System.out.println("printWithLambda " + name);
//    }
//
//    public void printWithReferences(String name) {
//        System.out.println("printWithReferences" + name);
//    }
//
//    public void printWithAnon(String name) {
//        System.out.println("printWithAnon" + name);
//    }
//
//    public void printWithObject(String name) {
//        System.out.println("printWithObject" + name);
//    }
//
//}
//public class App {
//    public static void main(String[] args) {
//
//        CommandImpl.printWithLambda("name");
//        Command com1 = String a -> return a;
//        com1.printWithAnon("sj");
//
//    }
//
//}
