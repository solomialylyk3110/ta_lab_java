package Task4;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) {
        List<String> list = Arrays.asList(new String[]{"w e e", "s a w"});
        List<String> stringList = list.stream()
                .flatMap(e -> Stream.of(e.split(" ")))
                .collect(Collectors.toList());
        System.out.println(stringList);
        long count = stringList.stream()
                .map(s -> s.toLowerCase())
                .distinct()
                .count();
        System.out.println("Count of unique: " + count);
        List<String> sortedList = stringList.stream()
                .distinct()
                .sorted()
                .collect(Collectors.toList());
        System.out.println("Sorted of unique: " + sortedList);
        for (String str : sortedList) {
            System.out.print(str + " n= ");
            long num = stringList.stream()
                    .filter(s -> s.equals(str))
                    .count();
            System.out.println(num);
        }
    }
}
