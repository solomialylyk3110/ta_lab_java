package Task3;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Application {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 3, 6, 3, 7);
        List<Integer> list1 = Stream.generate(new Random()::nextInt)
                .limit(5).collect(Collectors.toList());
        System.out.println(list1);

        long count = list1.stream().count();
        long max = list1.stream().max(Integer::compare).get();
        long min = list1.stream().min(Integer::compare).get();
        Optional<Integer> sum = list1.stream().reduce((i, j) -> i + j);
        long sum1 = list1.stream().mapToInt(Integer::intValue).sum();
        OptionalDouble avar = list1.stream().mapToInt(i -> i).average();
        long countMoreAvarage = list1.stream().filter(n -> n > avar.getAsDouble()).count();
        System.out.println("Count: " + count);
        System.out.println("Max: " + max);
        System.out.println("Min: " + min);
        System.out.println("Sum: " + sum);
        System.out.println("Sum: " + sum1);
        System.out.println("Avarage: " + avar);
        System.out.println("Numbers bigger than avarage: " + countMoreAvarage);
    }
}
