package Task1;

import static Task1.Lambdas.maxValue;

public class Application {
    public static void main(String[] args) {
        MyLambda lambda = (a, b, c) -> maxValue(a, b, c);
        System.out.println("Max: "+ lambda.fun1(1, 2, 3));

        MyLambda lambda1 = (a, b, c) -> (a + b + c) / 3;
        System.out.println("AVG: "+ lambda1.fun1(1, 2, 3));
    }
}
