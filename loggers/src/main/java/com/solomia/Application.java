package com.solomia;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Application {
    private static Logger logger1= LogManager.getLogger(Application.class);
    public static void main(String[] args) {
        MyClass my= new MyClass("Solomia");
        my.toString();

        logger1.error("Exception", new IOException("error massage"));
        logger1.trace("This is an trace massage");
        logger1.debug("This is an debug massage");
        logger1.info("This is an info massage");
        logger1.warn("This is an warn massage");
        logger1.error("This is an error massage");
        logger1.fatal("This is an fatal massage");


    }
}
