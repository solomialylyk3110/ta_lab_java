package com.solomia;

public class MyClass {
    String name;

    public MyClass(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "MyClass{" +
                "name=" + name +
                '}';
    }
}