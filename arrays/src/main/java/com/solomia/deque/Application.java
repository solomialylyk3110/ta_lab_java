package com.solomia.deque;

public class Application {
    public static void main(String[] args) {
        Deque deque = new Deque();
        deque.addFirst(2);
        deque.addFirst(3);
        deque.addFirst(4);
        deque.addLast(1);
        System.out.println(deque);

    }
}

