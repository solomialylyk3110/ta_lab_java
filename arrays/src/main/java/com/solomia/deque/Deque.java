package com.solomia.deque;

import java.util.ArrayList;
import java.util.List;

public class Deque {
    private static int count;
    private List<Integer> list;

    public Deque() {
        count = 0;
        list = new ArrayList<>();
    }

    public boolean isEmpty() {
        return count == 0;
    }

    public void addFirst(int value) {
        list.add(0,value);
        count++;
    }

    public void addLast(int value) {
        list.add(list.size(), value);
        count++;
    }

//    public boolean offerFirst(int value) {
//        addFirst(value);
//        return true;
//    }
//
//    public boolean offerLast(int value) {
//        addLast(value);
//        return true;
//    }

    public int pollFirst() {
        int deleted = getFirst();
        list.remove(0);
        return deleted;
    }

    public int pollLast() {
        int deleted = getLast();
        list.remove(list.size()-1);
        return deleted;
    }

    public int  getFirst() {
        return list.get(0);
    }

    private int getLast() {
        return list.get(list.size()-1);
    }

    @Override
    public String toString() {
        return "Deque{" +
                "count=" + count +
                ", list=" + list +
                '}';
    }
}
