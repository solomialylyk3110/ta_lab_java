package com.solomia.logictask;

public class ThirdTask {

    public  static void simpleRemovingDuplicates(int []arr){ // прост видаляє дублікати
        int size=arr.length;
        for (int i = 0; i < size; i++) {
            for (int j = 1; j < size - i; j++) {
                if (arr[j - 1] > arr[j]) {
                    int temp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = temp;
                }
            }
        }
        int compare = 0;
        arr[compare] = arr[0];

        for (int i = 1; i < size; i++) {
            if (arr[compare] != arr[i]) {
                compare++;
                arr[compare] = arr[i];
            }
        }

        System.out.println('\n'+"Array after removing duplicate elements is :");
        for (int i = 0; i <= compare; i++) {
            System.out.print(arr[i] + " ");
        }
    }
}
