package com.solomia.logictask;

import static com.solomia.logictask.FirstTask.inBothArrays;
import static com.solomia.logictask.FirstTask.notInBothArrays;
import static com.solomia.logictask.SecondTask.deleteRepeated;
import static com.solomia.logictask.SecondTask.repeateMoreThanTwo;
import static com.solomia.logictask.ThirdTask.simpleRemovingDuplicates;

public class Application {
    static final int []array = {1,4,3,5,6,7,12,14,2};
    static final int []array1 = {11,4,2,4,10,4,2,9,2};



    public static void main(String[] args) {

        System.out.println("In both arrays");
        int []inBoth= inBothArrays(array, array1);
        for (Integer i: inBoth) {
            System.out.print(i+ " " );
        }

        System.out.println('\n'+"Not in both arrays");
        int []notInBoth= notInBothArrays(array, array1);
        for (Integer i: notInBoth) {
            System.out.print(i+ " " );
        }


        System.out.println('\n'+"Repeated more than 2: ");
        int [] rep= repeateMoreThanTwo(array1);
        for (Integer i: rep) {
            System.out.print(i+ " ");
        }
        System.out.println('\n'+"Array with  deleted repeat: ");
        int [] del= deleteRepeated(rep,array1);
        for (Integer i: del) {
            System.out.print(i+ " ");
        }

        simpleRemovingDuplicates(array1);

    }
}
