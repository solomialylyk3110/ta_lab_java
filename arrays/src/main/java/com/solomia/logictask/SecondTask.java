package com.solomia.logictask;

import java.util.Arrays;

public class SecondTask {

    public  static int [] repeateMoreThanTwo(int []arr){ // створює масив з елементів які повторюються
        int size=0;
        int []new_array=new int[arr.length];
        int found, flag, i, j;
        for (i = 0, found = 0; i < arr.length; i++) {
            for (j = 0, flag = 0; j < arr.length; j++) {
                if (arr[i] == arr[j] && i != j) {
                    flag++;
                }
            }

            if (flag >= 2) {
                for (j = 0, flag = 0; j < found; j++) {
                    if (new_array[j] == arr[i]) {
                        flag = 1;
                    }
                }
                if (flag == 0) {
                    new_array[found] = arr[i];
                    found++;
                    size++;
                }
            }
        }
        int []result= Arrays.copyOf(new_array, size);

        return  result;
    }

    public static int[] deleteRepeated(int []repeat, int []arr){ // поки не працює
        int []result= new int[arr.length];
       // int [] repeteated = repeateMoreThanTwo(arr);
        boolean check=false;
        int k=0;
        for(int i=0; i<arr.length; i++) {
            for (int j =0; j<repeat.length; j++){
                if(arr[i]==repeat[j]) {
                    check=true;
                }
            }
            if(check == false) {
                result[k++] = arr[i];
            }
            check =false;
        }
        int []finalResult = Arrays.copyOf(result, k);
        return finalResult;
    }
}
