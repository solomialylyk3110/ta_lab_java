package com.solomia.logictask;

import java.util.Arrays;

public class FirstTask {

    public static int [] inBothArrays(int []arr, int []arr1){
        int [] result = new int[arr.length];
        int iForResult=0;
        for (int i = 0; i < arr.length; i++){
            for (int j = 0; j < arr1.length; j++ ) {
                if (arr[i] == arr1[j]) {
                    result[iForResult]=arr[i];
                    iForResult++;
                }
            }
        }
        int []finalResult = Arrays.copyOf(result, iForResult);
        return finalResult;
    }

        public static int [] notInBothArrays(int []arr, int []arr1){
        int [] result = new int[arr.length+arr1.length];
        int [] inBothArrays = inBothArrays(arr, arr1);
        int k=0;
        boolean ones = false;
            for (int i = 0; i < arr.length; i++){
                for (int j = 0; j < inBothArrays.length; j++ ) {
                    if(arr[i]==inBothArrays[j]) {
                        ones= true;
                    }
                }
                if(ones == false) {
                    result[k++]= arr[i];
                }
                ones = false;

            }
            ones = false;
            for (int i = 0; i < arr1.length; i++){
                for (int j = 0; j < inBothArrays.length; j++ ) {
                    if(arr1[i]==inBothArrays[j]) {
                        ones= true;
                    }
                }
                if(ones == false) {
                    result[k++]= arr1[i];
                }
                ones = false;

            }
       int []finalResult = Arrays.copyOf(result, k);
        return finalResult;
    }
}
