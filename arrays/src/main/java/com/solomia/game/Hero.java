package com.solomia.game;

public class Hero {
    private static int power = 25;

    public Hero() {
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        power = power;
    }

    @Override
    public String toString() {
        return "Hero{" +
                "power=" + power +
                '}';
    }

    public int addPower(int score) {
        power += score;
        return power;
    }

    public int subtractionPower(int score) {
        power -= score;
        return power;
    }
}
