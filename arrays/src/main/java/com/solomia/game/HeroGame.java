package com.solomia.game;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HeroGame {
    private static Logger logger1 = LogManager.getLogger(HeroGame.class);
    private Hero hero = new Hero();
    private static final Random generator = new Random();
    private final int COUNT_DOORS = 10;
    static int count =0;
    int[] doors;
    boolean[] openDoors;

    public HeroGame() {
        doors = new int[COUNT_DOORS];
        openDoors = new boolean[COUNT_DOORS];
    }

    private void createDoor() {
        for (int i = 0; i < doors.length; i++) {
            int loop = getRandomInRange(0, 1);
            if (loop == 0) {
                doors[i] = getRandomInRange(-100, -5);
            } else {
                doors[i] = getRandomInRange(10, 80);
            }
        }
    }

    private void showDoors() {
        System.out.println();
        logger1.info("Room doors power > ");
        Arrays.stream(doors).forEach(i -> System.out.print(i + " "));
        System.out.println();
    }

    public void startGame() {
        Scanner scanner = new Scanner(System.in);
        createDoor();
        showDoors();
        while (true) {
            logger1.info("Please choose door");
            int scan;
            while (true) {
                scan = scanner.nextInt();
                if (scan < 1) {
                    logger1.info("Incorrect door. New attempt ^_^");
                } else {
                    break;
                }
            }
            chooseDoor(scan);
            logger1.info("Health of hero:" + hero.getPower());
            if (hero.getPower() <= 0) {
                logger1.info("You are dead!");
                break;
            }
        }
    }

    private void chooseDoor(int numberDoor) {
        if (openDoors[numberDoor] == true) {
            logger1.info("Door was opened before!");
        } else {
            hero.addPower(doors[--numberDoor]);
            logger1.info("Door # " + ++numberDoor + " is opening!");
            openDoors[numberDoor] = true;
        }

    }

    private static int getRandomInRange(int start, int end) {
        return start + generator.nextInt(end - start + 1);
    }

    public void countDeadDoors() {
        int count = 0;
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] < 0) {
                count++;
            }
        }
        logger1.info("Dead doors: " + count);
    }

    public int countRecursive(int i) {
        if (i<doors.length) {
            if ( doors[i]<0) {
                count++;
            }
            return countRecursive(++i);
        }
        return count;
    }

    public void countLiveDoors() {
        int count = 0;
        logger1.info("Live doors number: ");
        for (int i = 0; i < doors.length; i++) {
            if (doors[i] > 0) {
                count++;
                logger1.info(i + " ");
            }
        }
        logger1.info("Live doors = " + count);
    }
}
