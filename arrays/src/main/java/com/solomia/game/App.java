package com.solomia.game;

public class App {
    public static void main(String[] args) {
        HeroGame heroGame=new HeroGame();
        heroGame.startGame();
        heroGame.countDeadDoors();
        System.out.println(heroGame.countRecursive(0));
    }
}
