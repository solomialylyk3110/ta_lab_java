package com.solomia.container;


import java.util.ArrayList;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        long start = System.nanoTime();
        Container container = new Container();
        container.addString(" String1 ");
        System.out.println(container);
        container.addString(" String2 ");
        container.addString(" String3 ");
        System.out.println(container);
        long finish = System.nanoTime();
        long result = finish-start;
        System.out.println("Time for working with arrays " +result);
        start = System.nanoTime();
        List<String> list = new ArrayList<>();
        list.add("Str1");
        list.add("Str2");
        list.add("Str3");
        finish = System.nanoTime();
        result = finish-start;
        System.out.println("Time for working with ArrayList " +result);
    }
}
