package com.solomia.container;

import java.util.Arrays;

public class Container {
    private String[] array;
    private int count;

    public Container() {
        array  = new String[1];
        count = 0;
    }

    private void addFirst(String s){
    array[0] = s;
    count++;
}

    public void addString(String s) {
        if (count == 0 ) {
            addFirst(s);
        } else {
            array = Arrays.copyOf(array, array.length + 1);
            count++;
            array[array.length - 1] = s;
        }
    }

    public String[] getArray() {
        return array;
    }

    public void getString() {
        System.out.println(Arrays.toString(array));
    }

    @Override
    public String toString() {
        return "Container{" +
                "array=" + Arrays.toString(array) +
                ", count=" + count +
                '}';
    }
}
