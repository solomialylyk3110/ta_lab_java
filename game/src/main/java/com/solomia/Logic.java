package com.solomia;

import java.util.Scanner;

public class Logic extends Step{

    public Logic() {
        Scanner sc = new Scanner(System.in);
        while (true) {
            obj.show();
            String str = sc.nextLine();
            if (!str.equals("")) {
                String[] coors = str.split("x");
                if (coors.length == 2) {
                    try {
                        int userX = Integer.parseInt(coors[0]);
                        int userY = Integer.parseInt(coors[1]);
                        obj.setUserCoors(userX, userY);
                        int status = checkEmpty(userX, userY);
                        if (status > 0) {
                            boolean end = checkStop();
                            String message = "Finish. Winner is №" + obj.getActiveUser();
                            setStep();
                            obj.show();
                            if (end) {
                                System.out.println(message);
                                break;
                            }
                        } else if (status == -1) {
                            System.out.println("Out of boundary");
                        } else {
                            System.out.println("Place is occupied");
                        }
                    } catch (NumberFormatException e) {
                        System.out.println("Must be int number!");
                    }
                } else {
                    System.out.println("Format of coordinates (0x0)");
                }
            }
            getUserMessage();
        }
    }

    private  boolean checkStop() {

        String[] arr = new String[]{"up", "down", "left", "right", "up-left", "up-right", "down-left", "down-right"};

        for (int i = 0; i < arr.length; i++) {
            if (countSteps(obj.getUserX(), obj.getUserY(), arr[i], 0)) {
                return true;
            }
        }
        return false;
    }

    private void getUserMessage() {
        System.out.println("");
        System.out.print("Gamer №" + obj.getActiveUser()+ " - input coordinates: ");
    }

    private void setStep() {

        if (obj.getActiveUser() == 1) {
            obj.field[obj.getUserY()][obj.getUserX()] = getUserSymbol();
            obj.setActiveUser(2);
        } else if (obj.getActiveUser() == 2) {
            obj.field[obj.getUserY()][obj.getUserX()] = getUserSymbol();
            obj.setActiveUser(1);
        }
    }
}
