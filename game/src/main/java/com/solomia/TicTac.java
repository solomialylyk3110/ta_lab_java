package com.solomia;

public class TicTac {
    private int lenhth = 3;
    private int width = 3;
    public char[][] field;
    private int activeUser = 0;
    private int userX;
    private int userY;
    private int longStep = 3;

    public TicTac() {
        field = new char[getLenhth()][getWidth()];
        activeUser = 1;
    }

    public int getLenhth() {
        return lenhth;
    }

    public void setLenhth(int lenhth) {
        this.lenhth = lenhth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getUserX() {
        return userX;
    }

    public int getUserY() {
        return userY;
    }

    public void setActiveUser(int activeUser) {
        this.activeUser = activeUser;
    }

    public char[][] getField() {
        return field;
    }

    public int getLongStep() {
        return longStep;
    }

    public void show() {
        System.out.println();
        System.out.print("  ");
        for (int i = 0; i < field[0].length; i++) {
            System.out.print("" + i);
        }
        System.out.println("");
        //-------------------------------
        for (int i = 0; i < field.length; i++) {
            System.out.print(i + "|");

            for (int j = 0; j < field[i].length; j++) {

                if (field[i][j] != 0) {
                    System.out.print("" + field[i][j]);
                } else {
                    System.out.print("_");
                }

            }
            System.out.println("");
        }
    }

    public void setUserCoors(int x, int y) {
        userX = x;
        userY = y;
    }


    public int getActiveUser() {
        return activeUser;
    }

}
