package com.solomia;

public class Checking {
    protected TicTac obj = new TicTac();

    protected int checkEmpty(int x, int y) {
        int status = 0;
        if ((x >= 0 && y >= 0) && (x < obj.getWidth() && y < obj.getLenhth())) {
            if (obj.field[y][x] == 0) {
                status = 1;
            }
        } else {
            status = -1;
        }
        return status;
    }

    protected char getUserSymbol() {
        if (obj.getActiveUser() == 1) {
            return '0';
        } else if (obj.getActiveUser() == 2) {
            return 'X';
        }

        return ' ';
    }
}
