package com.solomia;

public class Step extends Checking {

    private boolean up(int x, int y, String way, int count) {
        if (checkEmpty(x, y - 1) >= 0) {
            if (obj.field[y - 1][x] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x, y - 1, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean upLeft(int x, int y, String way, int count) {
        if (checkEmpty(x - 1, y - 1) >= 0) {
            if (obj.field[y - 1][x - 1] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x - 1, y - 1, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean upRight(int x, int y, String way, int count) {
        if (checkEmpty(x + 1, y - 1) >= 0) {
            if (obj.field[y - 1][x + 1] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x + 1, y - 1, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean down(int x, int y, String way, int count) {
        if (checkEmpty(x, y + 1) >= 0) {
            if (obj.field[y + 1][x] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x, y + 1, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean downLeft(int x, int y, String way, int count) {
        if (checkEmpty(x - 1, y + 1) >= 0) {
            if (obj.field[y + 1][x - 1] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x - 1, y + 1, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean downRight(int x, int y, String way, int count) {
        if (checkEmpty(x + 1, y + 1) >= 0) {
            if (obj.field[y + 1][x + 1] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x + 1, y + 1, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean left (int x, int y, String way, int count) {
        if (checkEmpty(x - 1, y) >= 0) {
            if (obj.field[y][x - 1] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x - 1, y, way, count + 1);
                }
            }
        }
        return false;
    }

    private boolean right(int x, int y, String way, int count) {
        if (checkEmpty(x + 1, y) >= 0) {
            if (obj.field[y][x + 1] == getUserSymbol()) {
                if (count + 1 == obj.getLongStep() - 1) {
                    return true;
                } else {
                    return countSteps(x + 1, y, way, count + 1);
                }
            }
        }
        return false;
    }

    protected boolean countSteps(int x, int y, String way, int count) {
        switch (way) {
            case "up":
                up(x, y, way, count);
                break;
            case "up-left":
                upLeft(x, y, way, count);
                break;
            case "up-right":
                upRight(x, y, way, count);
                break;
            case "down":
                down(x, y, way, count);
                break;
            case "down-left":
                downLeft(x, y, way, count);
                break;
            case "down-right":
                downRight(x, y, way, count);
                break;
            case "left":
                left(x,y,way,count);
                break;

            case "right":
                right(x,y,way,count);
                break;
        }
        return false;
    }

}
