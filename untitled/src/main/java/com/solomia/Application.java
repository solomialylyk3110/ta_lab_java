package com.solomia;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Application {
    public static void main(String[] args) {
        //wildcards are needed for bounding, not adding
        ProductParent productParent = new ProductParent();
        Product product = new Product();
        Product productChild1 = new ProductChild1();

        List<? super Product> list = new  ArrayList <Product> ();
        list.add(product);
        list.add(productChild1);
        //list.add(productParent);
        Integer mas[]  ={1,2,3};
        System.out.println("Untitled");
        List<Integer> list1 = Arrays.asList(mas); //create not changing mas, so we can add or remove
        list1.remove(0);
    }
}
